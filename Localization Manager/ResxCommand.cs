﻿using EnvDTE;
using JetBrains.Annotations;
using Localization_Manager.Base;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Task = System.Threading.Tasks.Task;

namespace Localization_Manager
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class ResxCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0200;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("b390e235-724f-4222-a357-46859b418b29");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Localization_ManagerPackage package;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResxCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private ResxCommand(Localization_ManagerPackage package, OleMenuCommandService commandService)
        {
            this.package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new OleMenuCommand(this.Execute, menuCommandID);
            commandService.AddCommand(menuItem);
            Command = menuItem;
            Funcs = new Functions((IServiceProvider)ServiceProvider, this.package);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static ResxCommand Instance { get; private set; }
        public static OleMenuCommand Command { get; private set; }
        public static Functions Funcs { get; private set; }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(Localization_ManagerPackage package)
        {
            // Switch to the main thread - the call to AddCommand in ResxCommand's constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);
            
            OleMenuCommandService commandService = await package.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Instance = new ResxCommand(package, commandService);
            Command.BeforeQueryStatus += Instance.QueryCommandHandler;
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            // get active project of editing file
            var activeProject = Funcs.GetActiveProject();
            var paths = Funcs.ProjectFiles(activeProject).Select(x => Funcs.GetData(x)); // get all project files paths
            var rpath = paths.Where(x => Funcs.IsResourceFile(x.name)); // return paths of resx files
            var langs = rpath.Select(x => Funcs.GetLang(x.name)); // return languages

            if (rpath.Count() > 0) // only if at least one resx exist
            {
                // se requiere que el documento este guardado
                package.Dte.ActiveDocument.Save(); 
                
                // show window
                var keyText = Funcs.AroundText(package.Dte.ActiveDocument);
                Selection sel = Funcs.GetSelectionDoc(package.Dte.ActiveDocument);
                Funcs.ShowModalWindow(keyText, langs, rpath, package.Dte.ActiveDocument.FullName, sel);
            }
            else
            {
                Funcs.msg(activeProject.Name, "Resx Files doesn't exist", OLEMSGICON.OLEMSGICON_CRITICAL);
            }
        }

        // esto es para determinar si se ve o no el command, y para su texto. Tambien se puede usar enable
        private void QueryCommandHandler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (!(sender is OleMenuCommand menuCommand))
                return;

            menuCommand.Text = "Localize [Save file]"; // for dynamic text, this override xml buttontext
            menuCommand.Visible = menuCommand.Enabled = Funcs.CanMoveToResource(package.Dte.ActiveDocument); // si coincide el patrón
        }

    }
}
