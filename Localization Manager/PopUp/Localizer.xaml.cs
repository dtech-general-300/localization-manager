﻿using Localization_Manager.Base;
using Localization_Manager.Properties;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Localization_Manager.PopUp
{
    /// <summary>
    /// Interaction logic for Localizer.xaml
    /// </summary>
    public partial class Localizer : Window
    {
        // to: make window a modal Window

        private readonly IVsUIShell shell;
        public string OriginalkeyText { get; set; }
        public string currentPath { get; set; }
        public IEnumerable<(string name, string path)> resPaths { get; set; }
        public Functions Funcs { get; private set; }
        public Selection sel { get; set; }


        public Localizer(IVsUIShell uiShell, string keyText, IEnumerable<string> langs, IEnumerable<(string name, string path)> resPaths, Functions Funcs, string currentPath, Selection sel)
        {
            shell = uiShell;
            InitializeComponent();
            this.TitleBar.MouseDown += delegate { DragMove(); }; // allos user can drag popup
            this.OriginalkeyText = keyText; // save key text
            this.currentPath = currentPath; // current active document path
            ComboBoxLoad(langs); // initialize combobox, save langs there
            this.resPaths = resPaths; // save paths in the instance
            this.Funcs = Funcs; // to allow to use that methods
            this.sel = sel;
        }


        // COMBOBOX

        public class ComboData { public string Value { get; set; } }

        private void ComboBoxLoad(IEnumerable<string> langs)
        {
            List<ComboData> ListData = new List<ComboData>();
            foreach (string lang in langs)
            {
                ListData.Add(new ComboData { Value = lang });
            }
            cmbLanguage.ItemsSource = ListData;
            cmbLanguage.DisplayMemberPath = "Value";
            cmbLanguage.SelectedValuePath = "Value";
            cmbLanguage.SelectedValue = Settings.Default.Last_Language;
        }

        // OK

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            // required
            if (cmbLanguage.SelectedValue == null || cmbLanguage.SelectedValue?.ToString() == "")
            {
                Funcs.msg("Error!", "First select the language, remembet to copy \"Value\" in\nclipboard before you change it!", OLEMSGICON.OLEMSGICON_CRITICAL);
                return;
            }

            // create or update
            Funcs.SaveSingleOnResx(OriginalkeyText, Key.Text, Value.Text, cmbLanguage.SelectedValue as string, resPaths);

            // replace text with the new key
            Funcs.ReplaceNewKey(OriginalkeyText, Key.Text, currentPath, sel);

            // save last language & close
            Settings.Default.Last_Language = cmbLanguage.SelectedValue as string;
            Close();
            Funcs.msg("Success!", "All resx updated!");
        }


        // OTHERS

        private void cmbLanguage_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (resPaths != null)
            {
                Value.Text = Funcs.GetValueFrom(OriginalkeyText, cmbLanguage.SelectedValue as string, resPaths);
            }
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
                {
                    // MessageBox.Show("Control + Enter pressed");
                    Ok_Click(null, null);
                }
                else if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                {
                    // MessageBox.Show("Shift + Enter pressed");
                }
            }
        }
    }
}