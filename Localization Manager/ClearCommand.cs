﻿using System;
using System.ComponentModel.Design;
using System.Linq;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Task = System.Threading.Tasks.Task;

namespace Localization_Manager
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class ClearCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 420;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("9d714555-f163-4748-abf6-033c6827502a");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Localization_ManagerPackage package;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private ClearCommand(Localization_ManagerPackage package, OleMenuCommandService commandService)
        {
            this.package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new OleMenuCommand(this.Execute, menuCommandID);
            commandService.AddCommand(menuItem);
            Command = menuItem;
            Funcs = new Functions((IServiceProvider)ServiceProvider, this.package);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static ClearCommand Instance { get; private set; }
        public static OleMenuCommand Command { get; private set; }
        public static Functions Funcs { get; private set; }


        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(Localization_ManagerPackage package)
        {
            // Switch to the main thread - the call to AddCommand in ClearCommand's constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            OleMenuCommandService commandService = await package.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Instance = new ClearCommand(package, commandService);
            Command.BeforeQueryStatus += Instance.QueryCommandHandler;
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var res = Funcs.msg(
                "Eliminar registros de traducción", 
                "Are you sure?\nEn los archivos resx solo permanecerán\nlos registros que existan en el código.",
                OLEMSGICON.OLEMSGICON_QUERY, OLEMSGBUTTON.OLEMSGBUTTON_YESNO);
            
            if(res == 6) // User pressed YES button
            {
                // get selected project 
                var selectedProject = Funcs.GetSelectedProject();
                var paths = Funcs.ProjectFiles(selectedProject).Select(x => Funcs.GetData(x)); // get all project files paths
                var rpath = paths.Where(x => Funcs.IsResourceFile(x.name)); // return paths of resx files
                var pfiles = paths.Where(x => Funcs.IsValidFile(x)); // get all valid project files paths for translations

                // message box
                string message = "Unused registers deleted!";
                var icon = OLEMSGICON.OLEMSGICON_INFO;


                if (rpath.Count() > 0) // only if at least one resx exist
                {
                    Funcs.DelUnusedOnResx(Funcs.GetMatches(pfiles), rpath); // deleted unused on resx files
                }
                else
                {
                    message = "Resx Files doesn't exist";
                    icon = OLEMSGICON.OLEMSGICON_CRITICAL;
                }

                Funcs.msg(selectedProject.Name, message, icon);
            }
        }

        // esto es para determinar si se ve o no el command, y para su texto. Tambien se puede usar enable
        private void QueryCommandHandler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (!(sender is OleMenuCommand menuCommand))
                return;

            bool dirty = Funcs.FilesDirty(); // true significa que hay al menos 1 file sin guardar
            menuCommand.Text = dirty ? "Localization: Clear Unused (Save all files first)" : "Localization: Clear Unused"; 
            menuCommand.Visible = true;
            menuCommand.Enabled = !dirty;
        }

    }
}
