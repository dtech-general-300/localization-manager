﻿using EnvDTE;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Shell;
using System;

namespace Localization_Manager.Base
{
    // to get text selection
    
    public struct TextViewSelection
    {
        public TextViewPosition StartPosition { get; set; }
        public TextViewPosition EndPosition { get; set; }
        public string Text { get; set; }

        public TextViewSelection(TextViewPosition a, TextViewPosition b, string text)
        {
            StartPosition = TextViewPosition.Min(a, b);
            EndPosition = TextViewPosition.Max(a, b);
            Text = text;
        }
    }

    public struct TextViewPosition
    {
        private readonly int _column;
        private readonly int _line;

        public TextViewPosition(int line, int column)
        {
            _line = line;
            _column = column;
        }

        public int Line { get { return _line; } }
        public int Column { get { return _column; } }


        public static bool operator <(TextViewPosition a, TextViewPosition b)
        {
            if (a.Line < b.Line)
            {
                return true;
            }
            else if (a.Line == b.Line)
            {
                return a.Column < b.Column;
            }
            else
            {
                return false;
            }
        }

        public static bool operator >(TextViewPosition a, TextViewPosition b)
        {
            if (a.Line > b.Line)
            {
                return true;
            }
            else if (a.Line == b.Line)
            {
                return a.Column > b.Column;
            }
            else
            {
                return false;
            }
        }

        public static TextViewPosition Min(TextViewPosition a, TextViewPosition b)
        {
            return a > b ? b : a;
        }

        public static TextViewPosition Max(TextViewPosition a, TextViewPosition b)
        {
            return a > b ? a : b;
        }
    }



    // to check if menu has to be visible

    public class Selection
    {
        [NotNull]
        private readonly TextDocument _textDocument;

        private readonly FileCodeModel _codeModel;

        public Selection([NotNull] TextDocument textDocument, [NotNull] string line, FileCodeModel codeModel)
        {
            _textDocument = textDocument;
            Line = line;
            _codeModel = codeModel;
        }

        [NotNull]
        public VirtualPoint Begin
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return _textDocument.Selection.TopPoint;
            }
        }

        [NotNull]
        public VirtualPoint End
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return _textDocument.Selection.BottomPoint;
            }
        }

        public bool IsEmpty
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return Begin.EqualTo(End);
            }
        }

        public string Text
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return _textDocument.Selection?.Text;
            }
        }

        [NotNull]
        public string Line { get; }

        public string FunctionName
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return GetCodeElement(vsCMElement.vsCMElementFunction)?.Name;
            }
        }

        public string ClassName
        {
            get
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                return GetCodeElement(vsCMElement.vsCMElementClass)?.Name;
            }
        }

        public void MoveTo(int startColumn, int endColumn)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var selection = _textDocument.Selection;
            if (selection == null)
                return;

            selection.MoveToLineAndOffset(Begin.Line, startColumn);
            selection.MoveToLineAndOffset(Begin.Line, endColumn, true);
        }

        public void ReplaceWith(string replacement)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var selection = _textDocument.Selection;
            // using "selection.Text = replacement" does not work here, since it will trigger auto-complete,
            // and this may add unwanted additional characters, resulting in bad code.
            selection?.ReplaceText(selection.Text, replacement);
        }

        private CodeElement GetCodeElement(vsCMElement scope)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            try
            {
                return _codeModel?.CodeElementFromPoint(Begin, scope);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    
}
