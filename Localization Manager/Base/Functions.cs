﻿using EnvDTE;
using EnvDTE80;
using Localization_Manager.Base;
using Localization_Manager.PopUp;
using Microsoft.Internal.VisualStudio.PlatformUI;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Localization_Manager
{
    public class Functions
    {
        public Localization_ManagerPackage pckg { get; set; }
        private IServiceProvider serviceProvider { get; set; }
        private Regex rex = new Regex(@"(?<=LL\s*\[\s*@?\s*(""|'))([^\]]+)(?=(""|')\s*\])");

        public Functions(IServiceProvider sp, Localization_ManagerPackage lmpckg)
        {
            serviceProvider = sp;
            pckg = lmpckg;
        }





        // to get text selection

        public IWpfTextView GetTextView()
        {
            var textManager = serviceProvider.GetService<SVsTextManager, IVsTextManager2>();
            IVsTextView textView;
            textManager.GetActiveView2(1, null, (uint)_VIEWFRAMETYPE.vftCodeWindow, out textView);

            var componentModel = serviceProvider.GetService<SComponentModel, IComponentModel>();
            var editorAdaptersFactoryService = componentModel.GetService<IVsEditorAdaptersFactoryService>();
            return editorAdaptersFactoryService.GetWpfTextView(textView);
        }

        // recoge el texto seleccionado por el usuario
        public TextViewSelection GetSelection()
        {
            var textManager = serviceProvider.GetService<SVsTextManager, IVsTextManager2>();
            IVsTextView view;
            int result = textManager.GetActiveView2(1, null, (uint)_VIEWFRAMETYPE.vftCodeWindow, out view);

            view.GetSelection(out int startLine, out int startColumn, out int endLine, out int endColumn);//end could be before beginning
            var start = new TextViewPosition(startLine, startColumn);
            var end = new TextViewPosition(endLine, endColumn);

            view.GetSelectedText(out string selectedText);

            TextViewSelection selection = new TextViewSelection(start, end, selectedText);
            return selection;
        }

        // devuelve el path del archivo abierto
        public string GetActiveFilePath()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var dte = serviceProvider.GetService<DTE, EnvDTE80.DTE2>();
            return dte?.ActiveDocument.FullName;
        }

        public Project GetActiveProject()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var dte = serviceProvider.GetService<DTE, EnvDTE80.DTE2>();
            return dte.ActiveDocument.ProjectItem.ContainingProject;
        }

        public bool FilesDirty()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var dte = serviceProvider.GetService<DTE, EnvDTE80.DTE2>();
            bool dirty = false;
            foreach (Document doc in dte.Documents)
            {
                if (!doc.Saved)
                {
                    dirty = true;
                    break;
                }
            }
            return dirty;
        }

        public void ShowModalWindow(string keyText, IEnumerable<string> langs, IEnumerable<(string name, string path)> resPaths, string currentPath, Selection sel)
        {
            // get instance of xaml wpf window
            ThreadHelper.ThrowIfNotOnUIThread();
            IVsUIShell uiShell = serviceProvider.GetService<SVsUIShell, IVsUIShell>();
            var WinLoc = new Localizer(uiShell, keyText, langs, resPaths, this, currentPath, sel);

            //// override initial position with cursor position
            //var textView = this.GetTextView();
            //WinLoc.WindowStartupLocation = WindowStartupLocation.Manual;
            //WinLoc.Top = textView.Caret.Top;
            //WinLoc.Left = textView.ViewportLeft + textView.ViewportWidth / 2 - WinLoc.Width / 2;

            // override text of window
            WinLoc.Key.Text = keyText;
            WinLoc.Value.Text = GetValueFrom(keyText, WinLoc.cmbLanguage.SelectedValue as string, resPaths);

            // open the window
            IntPtr hwnd; //get the owner of this dialog
            uiShell.GetDialogOwnerHwnd(out hwnd);
            uiShell.EnableModeless(0);
            try
            {
                WindowHelper.ShowModal(WinLoc, hwnd);
            }
            finally
            {
                // this will take place after the window is closed.
                uiShell.EnableModeless(1);
            }
        }

        public string GetValueFrom(string key, string lang, IEnumerable<(string name, string path)> resPaths)
        {
            foreach (var resPath in resPaths)
            {
                if (IsMatching(lang, resPath.name))
                {
                    using (ResXResourceSet resxSet = new ResXResourceSet(resPath.path))
                    {
                        return resxSet.GetString(key);
                    }
                }
            }
            return "";
        }

        public bool IsMatching(string lang, string FileName)
        {
            var IsNeutral = lang == "Neutral" && FileName.ToLower() == "sharedresource.resx";
            var IsLang = lang != null && FileName.Contains(lang);
            return IsLang || IsNeutral;
        }




        // message box

        public int msg(
            string title,
            string message,
            OLEMSGICON icon = OLEMSGICON.OLEMSGICON_INFO,
            OLEMSGBUTTON button = OLEMSGBUTTON.OLEMSGBUTTON_OK,
            OLEMSGDEFBUTTON def = OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST)
        {
            var res = VsShellUtilities.ShowMessageBox(pckg, message, title, icon, button, def);
            return res;
            // OK = 1, Cancel = 2, Abort = 3, Retry = 4, Ignore = 5, Yes = 6, No = 7
        }




        // to check if menu has to be visible

        public bool CanMoveToResource(Document document)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var extension = Path.GetExtension(document?.FullName);
            if (extension == null)
                return false;

            var selection = GetSelectionDoc(document);
            if (selection == null)
                return false;

            if (!selection.Begin.EqualTo(selection.End)) // esto no permite abrir el menu cuando se seleciona algo
                return false; // could make this true 

            var s = GetMatch(document.FullName, selection.Begin.Line, selection.Begin.LineCharOffset - 1);

            return s != null;
        }


        public string AroundText(Document document)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            var selection = GetSelectionDoc(document);
            return GetMatch(document.FullName, selection.Begin.Line, selection.Begin.LineCharOffset - 1);
        }

        public string GetMatch(string path, int line, int column)
        {
            // este metodo permite conocer cual match cuadra con la posicion del cursor y lo devuelve
            string text = File.ReadAllText(path, Encoding.UTF8);
            string[] lines = text.Split('\n');

            // current position se obtiene por sumar all lengths (+1 por el \n que borra Split) antes de la linea seleccionada y luego suma column
            var CurrentPosition = lines.Take(line - 1).Sum(x => x.Length + 1) + column;
            foreach (Match match in rex.Matches(text))
            {
                var EndPosition = match.Index + match.Length - 1;
                if (match.Index - 2 <= CurrentPosition && CurrentPosition <= EndPosition + 2) // si la posicion seleccionada esta entre el match, pero con tolerancia
                {
                    return match.Value;
                }
            }
            return null;
        }

        public void ReplaceNewKey(string originalKey, string newKey, string path, Selection sel)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            string text = File.ReadAllText(path, Encoding.UTF8);
            string[] lines = text.Split('\n');

            var line = sel.Begin.Line;
            var column = sel.Begin.LineCharOffset - 1;
            var CurrentPosition = lines.Take(line - 1).Sum(x => x.Length + 1) + column;

            foreach (Match match in rex.Matches(text))
            {
                var EndPosition = match.Index + match.Length - 1;
                if (match.Index - 2 <= CurrentPosition && CurrentPosition <= EndPosition + 2) // si la posicion seleccionada esta entre el match, pero con tolerancia
                {
                    text = rex.Replace(text, newKey, 1, match.Index - 5); // un poco antes del match reemplaza 1 solo con el new key
                    break;
                }
            }

            File.WriteAllText(path, text, Encoding.UTF8);
        }

        public Selection GetSelectionDoc(Document document)
        {
            // devuelve una clase que contiene el texto seleccionado por el usuario
            ThreadHelper.ThrowIfNotOnUIThread();

            var textDocument = (TextDocument)document.Object(@"TextDocument");

            var topPoint = textDocument?.Selection?.TopPoint;
            if (topPoint == null)
                return null;

            var line = textDocument.CreateEditPoint()?.GetLines(topPoint.Line, topPoint.Line + 1);
            if (line == null)
                return null;

            var fileCodeModel = document.ProjectItem?.FileCodeModel;

            return new Selection(textDocument, line, fileCodeModel);
        }





        // to get selected project

        public Project GetSelectedProject()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            IntPtr hierarchyPointer, selectionContainerPointer;
            Object selectedObject = null;
            IVsMultiItemSelect multiItemSelect;
            uint projectItemId;

            IVsMonitorSelection monitorSelection =
                (IVsMonitorSelection)Package.GetGlobalService(typeof(SVsShellMonitorSelection));

            monitorSelection.GetCurrentSelection(out hierarchyPointer,
                                                 out projectItemId,
                                                 out multiItemSelect,
                                                 out selectionContainerPointer);

            IVsHierarchy selectedHierarchy = Marshal.GetTypedObjectForIUnknown(
                                                 hierarchyPointer,
                                                 typeof(IVsHierarchy)) as IVsHierarchy;

            if (selectedHierarchy != null)
            {
                ErrorHandler.ThrowOnFailure(selectedHierarchy.GetProperty(
                                                  projectItemId,
                                                  (int)__VSHPROPID.VSHPROPID_ExtObject,
                                                  out selectedObject));
            }

            Project selectedProject = selectedObject as Project;
            return selectedProject;
        }





        // to get all files in a project

        private IEnumerable<ProjectItem> Recurse(ProjectItem item)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            yield return item;
            foreach (ProjectItem i_item in Recurse(item.ProjectItems))
            {
                yield return i_item;
            }
        }

        private IEnumerable<ProjectItem> Recurse(ProjectItems items)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            if (items != null)
            {
                foreach (ProjectItem item in items)
                {
                    foreach (ProjectItem k in Recurse(item))
                    {
                        yield return k;
                    }
                }

            }
        }

        public IEnumerable<ProjectItem> ProjectFiles(Project project)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            foreach (ProjectItem item in Recurse(project.ProjectItems))
            {
                yield return item;
            }
        }




        // files funtions

        public bool IsResourceFile(string name)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            return name.Contains("SharedResource.") && name.Contains("resx");
        }

        public bool IsValidFile((string name, string path) file)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            bool IsFolder = file.path.Last() == '\\';
            bool IsResource = IsResourceFile(file.name);
            bool IsImagePdf = IsImageOrPdf(file.name);
            return !IsFolder && !IsResource && !IsImagePdf; // es valido cuando no es una carpeta, resx, imagen o pdf
        }

        public (string name, string path) GetData(ProjectItem item)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            return (
                item.Name,
                item.Properties.Item("FullPath").Value.ToString()
            );
        }

        public bool IsImageOrPdf(string FileName)
        {
            IEnumerable<string> ImageExts = new List<string>() { "png", "ico", "gif", "jpg", "jpeg", "tiff", "pdf" };
            var itis = false;
            var OriginalExt = FileName.Split('.').Last();
            foreach (var ext in ImageExts)
            {
                if (ext == OriginalExt)
                {
                    itis = true;
                    break;
                }
            }
            return itis;
        }

        public List<string> GetMatches(IEnumerable<(string name, string path)> projectFiles)
        {
            // search in project files the all regex matches
            var matches = new List<string>();
            foreach (var file in projectFiles)
            {
                string text = File.ReadAllText(file.path, Encoding.UTF8);
                foreach (Match match in rex.Matches(text))
                {
                    matches.Add(match.Value);
                }
            }
            return matches.Distinct().ToList();
        }

        public string GetLang(string FileName)
        {
            var array = FileName.Split('.');
            if (array.Length == 2) { return "Neutral"; }
            return array[1];
        }





        // resx files funtions

        public void SaveOnResx(List<string> matches, IEnumerable<(string name, string path)> rpaths)
        {
            foreach (var rpath in rpaths)
            {
                using (ResXResourceWriter resxWriter = new ResXResourceWriter(rpath.path))
                {
                    using (ResXResourceReader resxReader = new ResXResourceReader(rpath.path))
                    {
                        var resxMap = new Dictionary<string, string>();
                        foreach (DictionaryEntry entry in resxReader)
                        {
                            resxMap.Add(entry.Key.ToString(), entry.Value.ToString());
                        }

                        foreach (var match in matches)
                        {
                            string value;
                            if (resxMap.ContainsKey(match)) // busca si existe
                            {
                                value = resxMap[match]; // le deja con el mismo valor
                            }
                            else
                            {
                                value = rpath.name == "SharedResource.resx" ? match : ""; // si no existe y es el resx Neutral se graba el key en el valor
                            }
                            resxWriter.AddResource(match, value); // siempre hay que usar porque resxWriter inicia un archivo limpio
                        }
                    }
                }
            }
        }

        public void SaveSingleOnResx(string originalKey, string key, string newValue, string lang, IEnumerable<(string name, string path)> rpaths)
        {
            foreach (var rpath in rpaths)
            {
                using (ResXResourceWriter resxWriter = new ResXResourceWriter(rpath.path))
                {
                    using (ResXResourceReader resxReader = new ResXResourceReader(rpath.path))
                    {
                        var resxMap = new Dictionary<string, string>();
                        bool containsKey = false;
                        foreach (DictionaryEntry entry in resxReader)
                        {
                            resxMap.Add(entry.Key.ToString(), entry.Value.ToString());

                            if (entry.Key.ToString() != originalKey) // evita grabar duplicado con originalkey
                            {
                                if (entry.Key.ToString() != key) // evita grabar duplicado con el key ( que sería el final key)
                                {
                                    resxWriter.AddResource(entry.Key.ToString(), entry.Value.ToString()); // copia todo excepto el key
                                }
                            }
                            else
                            {
                                containsKey = true; // indica que el key si existe en el archivo
                            }
                        }

                        string value;
                        if (containsKey) // si existe en el resx
                        {
                            // si existe en este archivo resx, miramos si este archivo cuadra o no con el lenguaje seleccionado
                            if (IsMatching(lang, rpath.name))
                            {
                                value = newValue; // si cuadra, actualizo el valor
                            }
                            else
                            {
                                // si no cuadra, le deja con el mismo valor, a menos que sean iguales (o sea que no se modificó previamente)
                                value = resxMap[originalKey] == originalKey ? key : resxMap[originalKey];
                            }
                        }
                        else // si no existe en el resx
                        {
                            if (IsMatching(lang, rpath.name))
                            {
                                value = newValue == "" && lang == "Neutral" ? key : newValue; // si cuadra, actualizo el valor excepto cuando es Neutral y no se especificó su valor: se pone key
                            }
                            else
                            {
                                value = rpath.name == "SharedResource.resx" ? key : ""; // si no cuadra, actualizo el valor en "" excepto en Neutral que tendrá key (porque es nuevo y no cuadra)
                            }
                        }
                        resxWriter.AddResource(key, value); // inserta el key, haya existido o no
                    }
                }
            }
        }

        public void DelUnusedOnResx(List<string> matches, IEnumerable<(string name, string path)> rpaths)
        {
            foreach (var rpath in rpaths)
            {
                using (ResXResourceWriter resxWriter = new ResXResourceWriter(rpath.path))
                {
                    using (ResXResourceReader resxReader = new ResXResourceReader(rpath.path))
                    {
                        var resxMap = new Dictionary<string, string>();
                        foreach (DictionaryEntry entry in resxReader)
                        {
                            resxMap.Add(entry.Key.ToString(), entry.Value.ToString());
                        }

                        foreach (var match in matches)
                        {
                            string value;
                            if (resxMap.ContainsKey(match)) // busca si existe
                            {
                                value = resxMap[match]; // si existe le deja con el mismo valor
                                resxWriter.AddResource(match, value); // recuerde que resxWriter inicia un archivo limpio
                            }
                        }
                    }
                }
            }
        }

    }

}
